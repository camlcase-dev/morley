0.2.0
=====

Initial release.

* Typechecker and interpreter for Michelson.
* Morley extensions:
  - syntax sugar
  - let-blocks
  - inline assertions
* EDSL for unit testing and integrational testing
